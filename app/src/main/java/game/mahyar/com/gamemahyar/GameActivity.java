package game.mahyar.com.gamemahyar;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class GameActivity extends AppCompatActivity {

    private static final String TAG = "GameActivity";

    private int num;
    private List<Integer> icons;
//    private int icons[] = {
//            R.drawable.a,
//            R.drawable.b,
//            R.drawable.c,
//            R.drawable.d,
//            R.drawable.e
//    };

    private GridView gridView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        icons = new ArrayList<>();
        icons.add(R.drawable.a);
        icons.add(R.drawable.b);
        icons.add(R.drawable.c);
        icons.add(R.drawable.d);
        icons.add(R.drawable.e);


        num = getIntent().getIntExtra("num", 3);
        gridView = (GridView) findViewById(R.id.gridView);
        gridView.setNumColumns(num);
        List<Integer> resi = prepareArray(num);

        final Adapter adapter = new Adapter(this, resi);
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.e(TAG, "onItemClick: " + view.toString());
            }
        });
    }

    private List<Integer> prepareArray(int n) {
        List<Integer> res = new ArrayList<>();
        List<Integer> copyIcons = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            Integer temp = (int) (Math.random() * n);
            Log.e(TAG, "prepareArray: " + temp);
            res.add(icons.get(temp));
            copyIcons.add(icons.get(temp));
            icons.remove(temp);
            n = icons.size();
        }
        int b = res.size();
        for (int i = 0; i < b; i++) {
            Integer temp = (int) (Math.random() * copyIcons.size());
            res.add(copyIcons.get(temp));
            copyIcons.remove(copyIcons.get(temp));
            b = copyIcons.size();
        }

        return res;
    }

}
