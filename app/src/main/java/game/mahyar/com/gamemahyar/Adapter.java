package game.mahyar.com.gamemahyar;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import java.util.List;

/**
 * Created by ehsan on 5/24/2018.
 */

public class Adapter extends BaseAdapter {

    private List<Integer> icons;
    private Activity activity;
    private ImageView imageView;

    public Adapter(Activity activity, List<Integer> icons) {
        this.icons = icons;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return icons.size();
    }

    @Override
    public Object getItem(int i) {
        return icons.get(i);
    }

    @Override
    public long getItemId(int i) {
        return /*icons.get(i)*/0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ImageView iview;
        if (view == null) {
            iview = new ImageView(activity.getApplicationContext());
            iview.setLayoutParams(new GridView.LayoutParams(150,150));
            iview.setScaleType(ImageView.ScaleType.CENTER_CROP);
//            iview.setPadding(5, 5, 5, 5);
        } else {
            iview = (ImageView) view;
        }
        iview.setImageResource(icons.get(i));
        return iview;
    }
}
